#!/bin/bash
echo "Improving '$1' with patches stated in '$2'..."
#pacaur -m $1 --noedit
PKGBUILDDIR="~/.cache/pacaur/$1/"

cd $PKGBUILDDIR
cp ~/.cache/pacaur/$1/PKGBUILD ~/.cache/pacaur/$1/PKGBUILDcachePatch

number=$(expr $(grep -n '^)$' ./PKGBUILD | cut -d: -f 1 | head -1) - 1)
patchList=$(sed "s/^/\t/" $2)
echo
echo "These patches will be installed last (before line $number):"
echo "$patchList"
echo
awk -v line="$number" -v text="\n$patchList" ' NR!=line{print} NR==line{print $0 text}' ~/.cache/pacaur/$1/PKGBUILD > ~/.cache/pacaur/$1/PKGBUILDcachePatch
read -p "Build package including these patches [Enter]"
makepkg --skipchecksums -p ~/.cache/pacaur/$1/PKGBUILDcachePatch -f -i
