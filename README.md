# cachePatcher

Dull cached package patcher. I use it to patch Wine.
I also suggest switching to the package sources' directory (cd ~/.cache/<packageName>) prior to execution to
 a) check that it exists,
 b) properly build from there.

cachePatch.sh <packageName> <path/to/patchFile>

_PatchFile_ can contain links to GitHub raw pages of patches.

Example use:

./cachePatch.sh wine-gaming-nine ~/Documents/winePatches.new

PatchFile prefect example:

https://raw.githubusercontent.com/hdmap/wine-hackery/master/f4se/mapimagetopdown.patch
https://raw.githubusercontent.com/hdmap/wine-hackery/master/f4se/getfreememstatecallbackfix.patch
